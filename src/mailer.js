const nodemailer = require("nodemailer");
let aws = require("@aws-sdk/client-ses");

const send = async (files) => {
  console.log(`Sending email`);
  console.log(`Files: ${files}`);

  const ses = new aws.SES({
    apiVersion: "2010-12-01",
    region: "us-east-1", // put an env variable here
    credentials: {
      secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
      accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    },
  });

  let transporter = nodemailer.createTransport({
    SES: { ses, aws },
  });

  let attachments = [];

  for (let i = 0; i < files.length; i++) {
    attachments[i] = {
      path: `${files[i]["filePath"]}/${files[i]["fileName"]}`,
    };
  }

  var mailDetails = {
    from: `"${process.env.FROM_NAME} 👻" <${process.env.FROM_EMAIL}>`,
    to: "mawandatracy@gmail.com",
    subject: "Statistics Report",
    attachments,
    text: `Testing email`,
  };

  try {
    let sendMailResponse = await transporter.sendMail(mailDetails);

    console.log("Message sent: %s", sendMailResponse.messageId);

    return true;
  } catch (e) {
    console.log(`Failed to send email`);
    console.log(e);

    return false;
  }
};

module.exports = {
  send,
};
