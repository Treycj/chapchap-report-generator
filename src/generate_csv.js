const fastcsv = require("fast-csv");
const fs = require("fs");
const pgp = require("pg-promise")({});
const QueryStream = require("pg-query-stream");
const CsvWriter = require("csv-write-stream");
const { DateTime } = require("luxon");

const db = pgp({
  user: process.env.DB_USER,
  host: process.env.DB_HOST,
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT,
  allowExitOnIdle: true,
});

const csv_generate = async (dateFrom, dateTo) => {
  let filePath = process.env.APP_ENV === "local" ? "." : "/tmp";

  const query1 = `SELECT COUNT(c.transaction_type) as total_transactions, SUM(c.amount) as total_amount, s.name, c.merchant_number as agent_number FROM 	cassandra_global_object as c INNER JOIN products_service_providers as p ON products_service_provider_id = p.id INNER JOIN service_providers as s ON p.service_provider_id = s.id WHERE time BETWEEN $1 AND $2 AND state='SUCCEEDED' AND c.products_service_provider_id NOT IN (344,343,345,237,238) GROUP BY s.name,c.amount,c.merchant_number ORDER BY c.merchant_number DESC`;

  const query2 = `SELECT COUNT(DISTINCT customer_phone_number)as customers FROM cassandra_global_object c WHERE time BETWEEN $1 AND $2 AND state='SUCCEEDED'`;

  let dt = DateTime.now();

  try {
    const csv1 = new CsvWriter();
    const csv2 = new CsvWriter();

    let fileName1 = `${dt.toUnixInteger()}_transactions_by_agent.csv`;
    let fileName2 = `${dt.toUnixInteger()}_customer_count.csv`;

    const file1 = fs.createWriteStream(`${filePath}/${fileName1}`, {
      flags: "w",
    });

    const file2 = fs.createWriteStream(`${filePath}/${fileName2}`, {
      flags: "w",
    });

    await db.stream(new QueryStream(query1, [dateFrom, dateTo]), (s) => {
      s.pipe(csv1).pipe(file1);
    });

    await db.stream(new QueryStream(query2, [dateFrom, dateTo]), (s) => {
      s.pipe(csv2).pipe(file2);
    });

    console.log("Created csv files ...");

    return [
      {
        fileName: fileName1,
        filePath,
      },
      {
        fileName: fileName2,
        filePath,
      },
    ];
  } catch (e) {
    console.log(e);

    return false;
  }
};

module.exports = {
  csv_generate,
};
