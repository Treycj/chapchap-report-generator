console.log(process.env.AWS_REGION);

const serverless = require("serverless-http");
const express = require("express");
const generate_csv = require("./src/generate_csv.js");
const upload_csv = require("./src/upload_csv.js");
const app = express();
const { DateTime } = require("luxon");
const mail = require("./src/mailer");

app.use(express.json());

app.get("/", (req, res, next) => {
  return res.status(200).json({
    message: "Hello from root!",
  });
});

app.post("/process", async (req, res, next) => {
  let body = req.body;

  if (!body.hasOwnProperty("dateFrom") || !body.hasOwnProperty("dateTo")) {
    return res.status(400).json({
      message: "Missing body parameters",
    });
  }

  let dateFrom = DateTime.fromMillis(body.dateFrom),
    dateTo = DateTime.fromMillis(body.dateTo);

  if (!dateFrom.isValid || !dateTo.isValid) {
    return res.status(400).json({
      message: "Invalid date(s) supplied",
    });
  }

  const files = await generate_csv.csv_generate(
    dateFrom.toMillis(),
    dateTo.toMillis()
  );

  if (!files) {
    return res.status(500).json({
      message: "error",
    });
  }

  const upload_process = await upload_csv.csv_upload(files);

  if (!upload_process) {
    return res.status(500).json({
      message: "error",
    });
  }

  // Send email
  await mail.send(files);

  return res.status(200).json({
    message: "success",
  });
});

app.use((req, res, next) => {
  return res.status(404).json({
    error: "Not Found",
  });
});

module.exports.handler = serverless(app);
