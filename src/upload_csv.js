const { PutObjectCommand } = require("@aws-sdk/client-s3");
const s3Client = require("./../lib/s3");
const fs = require("fs").promises;
const { DateTime } = require("luxon");

const csv_upload = async (files) => {
  const dt = DateTime.now();

  try {
    console.log("Beginning upload ...");

    for (const file of files) {
      let params = {
        Bucket: process.env.S3_BUCKET,
        Key: `${dt.year}/${dt.month}/${dt.day}/${file["fileName"]}`,
        Body: await fs.readFile(`${file["filePath"]}/${file["fileName"]}`),
      };

      let uploadResponse = await s3Client.send(new PutObjectCommand(params));

      console.log(uploadResponse);
    }

    return true;
  } catch (e) {
    console.log(e);

    return false;
  }
};

module.exports = {
  csv_upload,
};
